# README #

An example of input and output to and from the console and a file.

### How do I get set up? ###

Put *in.txt* in the 

* *python* forlder for Python
* *cpp\build\win32_vs2015\file* for C++
* *java/out/production/java/com/maks/input_output* or *java/out/production/java* for Java

Use [*IntelliJ IDEA*][1] for Java, [*PyCharm*][1] for Python and [*Visual Studio*][2] (or any other C++ IDE) with [cmake][3] for C++.

[1]: https://www.jetbrains.com/
[2]: https://www.visualstudio.com/
[3]: https://cmake.org/