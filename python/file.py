"""
An example of reading and writing to a file. 
"""

def main():
    input_file_name = "in.txt"
    output_file_name = "out.txt"

    try:
        with open(input_file_name, "r") as infile, open(output_file_name, "w") as outfile:
            data = infile.read()

            print("data = {}".format(data))

            outfile.write(data)
    except FileNotFoundError:
        print("file not found: {}".format(input_file_name))


if __name__ == "__main__":
    main()