"""
An example of reading and writing to the console. 
"""

def main():
    input_str = input()
    print("input_str = {}".format(input_str))


if __name__ == "__main__":
    main()