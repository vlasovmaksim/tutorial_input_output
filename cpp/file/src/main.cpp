///
/// An example of reading and writing to a file. 
///

int main() {
  std::string input_file_name = "in.txt";
  std::string output_file_name = "out.txt";

  std::ifstream infile;
  std::ofstream outfile;

  infile.open(input_file_name, std::ios::in);
  outfile.open(output_file_name, std::ios::out | std::ios::trunc);

  std::string data;

  if (infile.is_open()) {
    while (infile >> data) {
      std::cout << data << " ";

      outfile << data << " ";
    }

    infile.close();
    outfile.close();
  } else {
    std::cerr << "file not found: " << input_file_name << std::endl;
  }

  return 0;
}
