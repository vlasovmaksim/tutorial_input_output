///
/// An example of reading and writing to the console. 
///

int main() {
  std::string input;

  std::cin >> input;

  std::cout << input;

  return 0;
}
