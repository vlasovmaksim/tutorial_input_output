///
/// An example of usage InputStream to read a data from a file.
///

package com.maks.input_output;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class FileExampleReadOnly {
    public static void main(String[] args) {
        System.out.println(FileExampleReadOnly.class.getCanonicalName());

        String input_file_name = "in.txt";


        // Read data from a file with help of InputStream.
        InputStream stream = FileExamle.class.getResourceAsStream(input_file_name);
        System.setIn(stream);


        try {
            Scanner in = new Scanner(System.in);


            ArrayList<String> collection = new ArrayList<>();

            // Read data and store it in the collection.
            while (in.hasNext()) {
                String input_data = in.next();

                collection.add(input_data);
            }

            // Output data.
            for (String item : collection) {
                System.out.println(item);
            }
        } catch (NullPointerException ex) {
            System.out.println("Check input file path!");

            return;
        }
    }
}
