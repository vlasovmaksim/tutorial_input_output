///
/// An example of usage Scanner to read a data from the console.
///

package com.maks.input_output;

import java.util.Scanner;

public class ConsoleExamle {
    public static void main(String[] args) {
        System.out.println(ConsoleExamle.class.getCanonicalName());

        String input_data;

        Scanner in = new Scanner(System.in);

        // Read input from console.
        input_data = in.next();
        System.out.println(input_data);
    }
}
