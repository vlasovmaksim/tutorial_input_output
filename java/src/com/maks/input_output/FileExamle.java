///
/// An example of usage Scanner to read a data from the console.
/// An example of usage InputStream to read a data from a file.
/// An example of usage File to read a data from a file.
/// An example of usage PrintWriter to write a data to a file.
///

package com.maks.input_output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class FileExamle {
    public static void main(String[] args) {
        System.out.println(ConsoleExamle.class.getCanonicalName());

        // Get path to the out\production\PROJECT_FOLDER
        String current_class_path = FileExamle.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        // File names
        String input_file_name = "in.txt";
        String output_file_name = "out.txt";

        ArrayList<String> collection = new ArrayList<>();

        // Read data from the console.
//        read_input_data_1(collection);

        // Read data from a file with help of InputStream.
//        read_input_data_2(input_file_name, collection);

        // Read data from a file with help of File.
        read_input_data_3(input_file_name, current_class_path, collection);

        write_output_data(current_class_path, output_file_name, collection);
    }

    // Read data from the console.
    // If you want to stop input in the IntelliJ IDEA
    // console then use Ctrl+D / ⌘+D on Mac.
    private static void read_input_data_1(ArrayList<String> collection) {
        Scanner in = new Scanner(System.in);
        parse_input(collection, in);
    }


    // Read data from a file with help of InputStream.
    // Get path to the src
    private static void read_input_data_2(String input_file_name,
                                          ArrayList<String> collection) {
        InputStream stream = FileExamle.class.getResourceAsStream(input_file_name);
        System.setIn(stream);
        Scanner in = new Scanner(System.in);
        parse_input(collection, in);
    }

    // Read data from a file with help of File.
    private static void read_input_data_3(String input_file_name,
                                          String current_class_path,
                                          ArrayList<String> collection) {
        File infile = new File(current_class_path + input_file_name);

        try (Scanner in = new Scanner(infile)) {
            parse_input(collection, in);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void write_output_data(String current_class_path,
                                          String output_file_name,
                                          ArrayList<String> collection) {
        File outfile = new File(current_class_path + output_file_name);

        try (PrintWriter writer = new PrintWriter(outfile)) {
            for (String item : collection) {
                writer.println(item);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Parse input data from Scanner.
    private static void parse_input(ArrayList<String> collection,
                                    Scanner in) {
        while (in.hasNext()) {
            String input_data = in.next();

            collection.add(input_data);

            System.out.println(input_data);
        }
    }
}
